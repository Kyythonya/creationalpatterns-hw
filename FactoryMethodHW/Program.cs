﻿using System;

namespace FactoryMethodHW
{
    public interface IPaper
    {
        string GetPaperType();
    }

    public class Perl : IPaper
    {
        public string GetPaperType()
        {
            return "\"Perl\"";
        }
    }

    public class Offset : IPaper
    {
        public string GetPaperType()
        {
            return "\"Offset\"";
        }
    }

    public abstract class Printer
    {
        public IPaper Paper { get; protected set; }

        public string Print()
        {
            return $"Print with paper type: {Paper.GetPaperType()}";
        }
    }

    public class Canon : Printer
    {
        public Canon()
        {
            Paper = new Perl();
        }
    }

    public class HP : Printer
    {
        public HP()
        {
            Paper = new Offset();
        }
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            var canonPrinter = new Canon();

            Console.WriteLine(canonPrinter.Print());

            Console.ReadLine();
        }
    }
}
