﻿using System;

namespace SingletonHW
{
    public class Connection
    {
        private static Connection _connection;

        private Connection() {}

        public static Connection Create()
        {
            if (_connection is null)
                _connection = new Connection();
            return _connection;
        }

        public static string Connect()
        {
            return "Connection was successfully established!";
        }

        public static string Disconnect()
        {
            return "Connection was closed!";
        }
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            var connectionFirst = Connection.Create();
            var connectionSecond = Connection.Create();

            if (connectionFirst.Equals(connectionSecond))
                Console.WriteLine("Status Code : 200");
            else
                Console.WriteLine("Status Code : 400");
            
            Console.ReadLine();
        }
    }
}
